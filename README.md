# Flectra Community / account-fiscal-rule

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[account_avatax_oca](account_avatax_oca/) | 2.0.4.0.1| Compute Sales Tax using the Avalara Avatax Service
[account_fiscal_position_partner_type](account_fiscal_position_partner_type/) | 2.0.1.0.0| Account Fiscal Position Partner Type
[account_avatax_sale_oca](account_avatax_sale_oca/) | 2.0.2.0.0| Sales Orders with automatic Tax application using Avatax
[account_product_fiscal_classification_test](account_product_fiscal_classification_test/) | 2.0.1.0.0| tests account_product_fiscal_classification module
[account_product_fiscal_classification](account_product_fiscal_classification/) | 2.0.1.0.1| Simplify taxes management for products
[account_avatax_exemption_base](account_avatax_exemption_base/) | 2.0.1.1.0|         This application allows you to add exemptions base to Avatax    
[account_multi_vat](account_multi_vat/) | 2.0.1.0.0|         Allows setting multiple VAT numbers on any partner and select the right one        depending on the fiscal position and delivery address of the invoice.
[account_avatax_exemption](account_avatax_exemption/) | 2.0.3.0.0|         This application allows you to add exemptions to Avatax    
[account_avatax_website_sale](account_avatax_website_sale/) | 2.0.2.0.0| Ecommerce Sales Orders require tax recalculation prior to payment.
[l10n_eu_oss](l10n_eu_oss/) | 2.0.1.1.0| L10n EU OSS


