{
    "name": "Avalara Avatax Connector for Ecommerce",
    "version": "2.0.2.0.0",
    "author": "Cybernexus Solutions, Odoo Community Association (OCA)",
    "summary": "Ecommerce Sales Orders require tax recalculation prior to payment.",
    "license": "AGPL-3",
    "category": "Website/Website",
    "website": "https://gitlab.com/flectra-community/account-fiscal-rule",
    "depends": ["account_avatax_sale_oca", "website_sale"],
    "data": [],
    "auto_install": True,
    "development_status": "Beta",
    "maintainers": ["cybernexus"],
}
