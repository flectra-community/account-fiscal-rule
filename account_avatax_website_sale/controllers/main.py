from flectra.http import request

from flectra.addons.website_sale.controllers.main import WebsiteSale


class AvataxWebsiteSale(WebsiteSale):
    def payment(self, **post):
        order = request.website.sale_get_order()
        order._avatax_compute_tax()
        return super(AvataxWebsiteSale, self).payment(**post)
